<br/>
<p align="center">
  <a href="https://github.com/geanrobotics/project-template">
    <img src="images/logo.jpeg" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Project README Template</h3>

  <p align="center">
    An Awesome ReadME Template To Jumpstart Your Projects!
    <br/>
    <br/>
    <a href="https://github.com/geanrobotics/project-template"><strong>Explore the docs »</strong></a>
    <br/>
    <br/>
    <a href="https://github.com/geanrobotics/project-template">View Demo</a>
    .
    <a href="https://github.com/geanrobotics/project-template/issues">Report Bug</a>
    .
    <a href="https://github.com/geanrobotics/project-template/issues">Request Feature</a>
  </p>
</p>


## Table Of Contents

* [About the Project](#about-the-project)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [License](#license)
* [Authors](#authors)
* [Acknowledgements](#acknowledgements)

## About The Project

![Screen Shot](images/screenshot.gif)

Describe the project here at a high level here.

## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

* npm

```sh
npm install npm@latest -g
```

### Installation

1. Get a free API Key at [https://example.com](https://example.com)

2. Clone the repo

```sh
git clone https://github.com/your_username_/Project-Name.git
```

3. Install NPM packages

```sh
npm install
```

## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_

## Roadmap

See the [open issues](https://github.com/geanrobotics/project-template/issues) for a list of proposed features (and known issues).

## License

Distributed under the MIT License. See [LICENSE](https://github.com/geanrobotics/project-template/blob/main/LICENSE.md) for more information.

## Authors

* **geanrobotics** - *phd in assistive robot navigation* - [geanrobotics](https://github.com/geanrobotics)

## Acknowledgements

* [Shaan Khan](https://github.com/ShaanCoding/) - Create README generator