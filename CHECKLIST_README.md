### Setting up a new project

- [ ] Reproducible environment
- [ ] Overleaf paper submodule
- [ ] Project README
- [ ] 3 branches - devel, experiments, release